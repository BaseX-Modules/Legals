<?php
defined('XOOPS_ROOT_PATH') || exit('XOOPS root path not defined');

/**
 * @param $module
 * @return bool
 */
function xoops_module_install_legals(&$module)
{
    $data_file = XOOPS_ROOT_PATH . '/modules/Legals/sql/mysql.legals.sql';
    $GLOBALS['xoopsDB']->queryF('SET NAMES utf8');
    if (!$GLOBALS['xoopsDB']->queryFromFile($data_file)) {
        $module->setErrors('Pre-set data were not installed');

        return true;
    }

    return true;
}

/**
 * @param       $module
 * @param  null $prev_version
 * @return bool
 */
function xoops_module_update_legals(&$module, $prev_version = null)
{
    return true;
}
