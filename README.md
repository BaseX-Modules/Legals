![alt B CMS](http://base-x.org/images/logo.png)
## Legals module for  [XOOPS CMS 2.5.8+](https://xoops.org)
[![Software License](https://img.shields.io/badge/license-GPL-brightgreen.svg?style=flat)](LICENSE)
[![Translations on Transifex](http://base-x.org/images/translations-transifex-blue.svg)](https://www.transifex.com/base-x)


Legals module for [XOOPS CMS](http://xoops.org) for adding a multi-page Legal section to your Website



Please visit us on http://base-x.org

_NOTE: we are NOT associated with the XOOPS Project._

